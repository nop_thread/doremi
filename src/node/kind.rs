//! Node kinds.

use crate::node::RenderType;

use uuid::{uuid, Uuid};

/// Node kind.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum NodeKind {
    /// Native node kind.
    Native(NativeNodeKind),
    /// Custom node kind.
    Custom(CustomNodeKind),
}

impl NodeKind {
    /// Creates an node kind that corresponds to the given ID.
    #[inline]
    #[must_use]
    pub const fn from_id(id: NodeKindId) -> Self {
        match NativeNodeKind::from_id(id) {
            Some(native) => Self::Native(native),
            None => Self::Custom(CustomNodeKind { id }),
        }
    }
}

impl From<NativeNodeKind> for NodeKind {
    #[inline]
    fn from(kind: NativeNodeKind) -> Self {
        Self::Native(kind)
    }
}

impl From<CustomNodeKind> for NodeKind {
    #[inline]
    fn from(kind: CustomNodeKind) -> Self {
        Self::Custom(kind)
    }
}

impl PartialEq<NativeNodeKind> for NodeKind {
    #[inline]
    fn eq(&self, other: &NativeNodeKind) -> bool {
        match self {
            Self::Native(this) => *this == *other,
            Self::Custom(_) => false,
        }
    }
}

/// Node kind ID.
// A wrapper type to prevent pre-1.0.0 `uuid::Uuid` type from being exposed.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct NodeKindId(pub Uuid);

/// Implements conversions between `NativeElemKind` and `NodeKindId`.
macro_rules! impl_native_kind_id_conversions {
    ($($variant:ident($uuid_str:literal): $const_name:ident),* $(,)?) => {
        impl NativeNodeKind {
            $(
            #[doc = concat!(
                "Node kind ID for the [`",
                stringify!($variant),
                "`][`Self::",
                stringify!($variant),
                "`] kind."
            )]
            const $const_name: NodeKindId = NodeKindId(uuid!($uuid_str));
            )*

            /// Returns the node kind ID.
            #[must_use]
            pub const fn kind_id(self) -> NodeKindId {
                let id = match self {
                    $(
                    Self::$variant => uuid!($uuid_str),
                    )*
                };
                NodeKindId(id)
            }

            /// Returns the native node kind for the given node kind ID.
            #[must_use]
            pub const fn from_id(id: NodeKindId) -> Option<Self> {
                match id {
                    $(
                    Self::$const_name => Some(Self::$variant),
                    )*
                    _ => None,
                }
            }
        }
    };
}

/// Node kind natively supported by `doremi` crate.
///
/// This enum type is non-exhaustive since the future version would support
/// more kinds natively.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum NativeNodeKind {
    /// Title of the containing block.
    BlockTitle,
    /// Code block.
    ///
    /// This corresponds to `<pre><code>` of HTML 5.
    CodeBlock,
    /// Inline code.
    ///
    /// This corresponds to `<code>` of HTML 5.
    CodeInline,
    /// Stress emphasis.
    ///
    /// This node works as a stress emphasis, which usually changes the
    /// meaning of a sentence. In other words, the interpretation of a sentence
    /// will change depending on where is emphasized.
    ///
    /// If you want an emphasis that simply marks the important part and you
    /// don't want the emphasis to change the meaning of a sentence, use
    /// [`EmphasisStrong`][`Self::EmphasisStrong`].
    ///
    /// This corresponds to `<em>` of HTML 5.
    EmphasisStress,
    /// Strong emphasis.
    ///
    /// This node marks the important content. This just marks the part,
    /// which have been already important without any explicit markup, as an
    /// important content.
    /// That said, presence and absence of markup by this node won't change
    /// the meaning of sentences.
    ///
    /// If you want an emphasis that changes the interpretation of a sentence,
    /// use [`EmphasisStress`][`Self::EmphasisStress`].
    ///
    /// This corresponds to `<strong>` of HTML 5.
    EmphasisStrong,
    /// Footnote definition.
    ///
    /// Note that the content of the node of this kind will be independent
    /// from the main content flow. In other words, the content of the node
    /// won't be expanded to the place the node exists.
    FootnoteDefinition,
    /// Footnote reference, or footnote mark.
    ///
    /// This corresponds to `<footnoteref>` of DocBook 5.2.
    FootnoteReference,
    /// Forced line break.
    ///
    /// The forced line break starts the new text line in the rendered output.
    /// This may also called as "hard break".
    ///
    /// This corresponds to `<br>` of HTML 5.
    ForcedLineBreak,
    /// Generic block division.
    ///
    /// This corresponds to `<div>` of HTML 5.
    GenericBlock,
    /// Generic inline span.
    ///
    /// This corresponds to `<span>` of HTML 5.
    GenericInline,
    /// Link.
    ///
    /// This corresponds to `<a>` of HTML 5.
    Link,
    /// List item.
    ///
    /// This corresponds to `<li>` of HTML 5.
    ListItem,
    /// Ordered list.
    ///
    /// This corresponds to `<ol>` of HTML 5.
    ListOrdered,
    /// Unordered list.
    ///
    /// This corresponds to `<ul>` of HTML 5.
    ListUnordered,
    /// Media object block.
    MediaBlock,
    /// Inline media object.
    MediaInline,
    /// Paragraph.
    ///
    /// This corresponds to `<p>` of HTML 5.
    Paragraph,
    /// Block quote.
    ///
    /// This corresponds to `<blockquote>` of HTML 5.
    QuoteBlock,
    /// Non-interpreted source in some format to be embedded.
    RawFragment,
    /// Section.
    ///
    /// This corresponds to `<section>` of HTML 5.
    Section,
    /// Table.
    ///
    /// This corresponds to `<table> of HTML 5.
    Table,
    /// Table body.
    ///
    /// This corresponds to `<tbody> of HTML 5.
    TableBody,
    /// Table entry (cell).
    ///
    /// This corresponds to `<td>` and `<th>` of HTML 5.
    TableEntry,
    /// Table footer.
    ///
    /// This corresponds to `<tfoot> of HTML 5.
    TableFooter,
    /// Table header.
    ///
    /// This corresponds to `<thead> of HTML 5.
    TableHeader,
    /// Table row.
    ///
    /// This corresponds to `<tr>` of HTML 5.
    TableRow,
    /// Text fragment.
    Text,
    /// Thematic break.
    ///
    /// This corresponds to `<hr>` of HTML 5.
    ThematicBreak,
    /// Transparent nodes container.
    ///
    /// This is a dummy kind for cases when a root node of a document root has
    /// no appropriate node kind.
    ///
    /// This does not add any special meanings to the nodes of this type, but
    /// simply makes the node "container" of other (possibly multiple) nodes.
    /// This would be useful when a document or a document fragment have
    /// multiple toplevel nodes, but no explicit root nodes.
    ///
    /// Note that nodes of this type might be replaced with their children.
    /// If they can be replaced, they should behave as if they are a generic
    /// blocks or generic inline nodes depending on the context.
    /// If you want to avoid this ambiguity and omissibility, you should
    /// proactively remove or replace the node of this kind where it does not
    /// need to be a transparent container.
    ///
    /// This may correspond to `<body>`, `<main>`, `<div>`, or `<span>` of
    /// HTML 5.
    TransparentContainer,
    /// Unprocessable node.
    ///
    /// This can be used by any stage of the document processing; when reader
    /// cannot process some of the input, when the modification failed and even
    /// cannot fallback due to inconsistent or unknown data. However, processors
    /// should do their best to fallback to known and generic node whenever
    /// they can.
    Unprocessable,
}

impl_native_kind_id_conversions! {
    BlockTitle("c8357781-9384-4320-abb8-01c4e4ce2811"): BLOCK_TITLE_ID,
    CodeBlock("4b42f8c9-d1d9-4776-acac-2ad589c8556b"): CODE_BLOCK_ID,
    CodeInline("d5425bb3-c5cb-49f3-ad94-bd041dcf30ef"): CODE_INLINE_ID,
    EmphasisStress("052f9b97-a547-4dae-8490-850b8da2feae"): EMPHASIS_STRESS_ID,
    EmphasisStrong("616dceac-5f82-4272-87ae-95b0e585b4d4"): EMPHASIS_STRONG_ID,
    FootnoteDefinition("9070fb20-fe32-4a70-923a-07aaa88a0946"): FOOTNOTE_DEFINITION_ID,
    FootnoteReference("ae9b5183-c159-441f-befd-df196ff3bc35"): FOOTNOTE_REFERENCE_ID,
    ForcedLineBreak("a398a697-e7e1-4bc1-8ee2-8750ee779b00"): FORCED_LINE_BREAK_ID,
    GenericBlock("3d2f63b0-3d65-462c-80ba-77973d893a11"): GENERIC_BLOCK_ID,
    GenericInline("fd1e57c8-53b0-41e6-8987-1fbb8411fe2a"): GENERIC_INLINE_ID,
    Link("325ecd0e-58ce-49f1-835d-cd8ec3f05991"): LINK_ID,
    ListItem("3a9c83f0-2d3d-49e8-9b1c-e150d33460db"): LIST_ITEM_ID,
    ListOrdered("332b46fc-3d3d-4d1c-a37a-26494da0c4d7"): LIST_ORDERED_ID,
    ListUnordered("c695a5e9-c4b0-4347-9263-b38a80aade7f"): LIST_UNORDERED_ID,
    MediaBlock("eed42d97-4913-41ae-86e9-ca184616b90c"): MEDIA_BLOCK_ID,
    MediaInline("368700c4-ac07-4686-b97e-0a1faef1eec0"): MEDIA_INLINE_ID,
    Paragraph("b67bf69c-7ec8-4510-aa2c-eff513b43900"): PARAGRAPH_ID,
    QuoteBlock("9df70a4b-4a76-4b33-8dd8-5d639772ab83"): QUOTE_BLOCK_ID,
    RawFragment("7d625207-2bb5-4696-8784-514e569dca0a"): RAW_FRAGMENT_ID,
    Section("094cf447-a057-4a18-b0f4-354f2df93ece"): SECTION_ID,
    Table("bb9630c7-e050-49ec-8bec-6af3e5ed3b50"): TABLE_ID,
    TableBody("9a0f0083-7949-44ec-a9fb-8b88c0e22ec3"): TABLE_BODY_ID,
    TableEntry("7f360411-4ece-4441-a1a8-2f19609c51a7"): TABLE_ENTRY_ID,
    TableFooter("941c3a4c-a90c-416e-831d-c2528116e327"): TABLE_FOOTER_ID,
    TableHeader("ced9a324-40a2-4cd4-a14d-17a2ec4fc16f"): TABLE_HEADER_ID,
    TableRow("c1942b85-61b8-46cd-b5bc-9f3ff1bd0e9b"): TABLE_ROW_ID,
    Text("d2a3d7e5-2787-47c0-b570-1eb390f8c840"): TEXT_ID,
    ThematicBreak("ab766eb2-1e34-4bc4-9200-9b0eeec7b769"): THEMATIC_BREAK_ID,
    TransparentContainer("2cfbfeb6-8115-4cc5-bd96-ceaeb4b18bef"): TRANSPARENT_CONTAINER_ID,
    Unprocessable("aa42af4e-bb16-4ad9-aba6-0cb111814dc7"): UNPROCESSABLE_ID,
}

impl NativeNodeKind {
    /// Returns the render type for the kind.
    #[must_use]
    pub fn render_type(self) -> RenderType {
        match self {
            Self::BlockTitle => RenderType::Block,
            Self::CodeBlock => RenderType::Block,
            Self::CodeInline => RenderType::Inline,
            Self::EmphasisStress => RenderType::Inline,
            Self::EmphasisStrong => RenderType::Inline,
            Self::FootnoteDefinition => RenderType::NotApplicable,
            Self::FootnoteReference => RenderType::Inline,
            Self::ForcedLineBreak => RenderType::Inline,
            Self::GenericBlock => RenderType::Block,
            Self::GenericInline => RenderType::Inline,
            Self::Link => RenderType::Contextual,
            Self::ListItem => RenderType::Block,
            Self::ListOrdered => RenderType::Block,
            Self::ListUnordered => RenderType::Block,
            Self::MediaBlock => RenderType::Block,
            Self::MediaInline => RenderType::Inline,
            Self::Paragraph => RenderType::Block,
            Self::QuoteBlock => RenderType::Block,
            Self::RawFragment => RenderType::Contextual,
            Self::Section => RenderType::Block,
            Self::Table => RenderType::Block,
            Self::TableBody => RenderType::Block,
            // TODO: Should `TableEntry` be `Block`, or other special render type?
            Self::TableEntry => RenderType::Block,
            Self::TableFooter => RenderType::Block,
            Self::TableHeader => RenderType::Block,
            Self::TableRow => RenderType::Block,
            Self::Text => RenderType::Inline,
            Self::ThematicBreak => RenderType::Block,
            Self::TransparentContainer => RenderType::Contextual,
            Self::Unprocessable => RenderType::NotApplicable,
        }
    }

    /// Returns `true` if this is a sectioning content or a sectioning root.
    #[must_use]
    pub(crate) fn is_sectioning_content_or_root(self) -> bool {
        // Intentionally not using `matches!()` or `_` wildcard, in order to
        // prevent the condition from being outdated when new kinds are added.
        match self {
            Self::QuoteBlock | Self::Section => true,
            Self::BlockTitle
            | Self::CodeBlock
            | Self::CodeInline
            | Self::EmphasisStress
            | Self::EmphasisStrong
            | Self::FootnoteDefinition
            | Self::FootnoteReference
            | Self::ForcedLineBreak
            | Self::GenericBlock
            | Self::GenericInline
            | Self::Link
            | Self::ListItem
            | Self::ListOrdered
            | Self::ListUnordered
            | Self::MediaBlock
            | Self::MediaInline
            | Self::Paragraph
            | Self::RawFragment
            | Self::Table
            | Self::TableBody
            | Self::TableEntry
            | Self::TableFooter
            | Self::TableHeader
            | Self::TableRow
            | Self::Text
            | Self::ThematicBreak
            | Self::TransparentContainer
            | Self::Unprocessable => false,
        }
    }
}

/// Custom node kind.
///
/// `CustomNodeKind` cannot be directly created from [`NodeKindId`],
/// since the kind can be natively supported in newer implementations.
/// Use [`NodeKind::from_id`] method to create an node kind value.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct CustomNodeKind {
    /// Unique ID for the node kind.
    id: NodeKindId,
}
