//! Document

use crate::node::{ContentData, ContentNode, ContentTree, NativeNodeKind};

/// Document.
#[derive(Debug, Clone)]
pub struct Document {
    /// Content tree.
    content: ContentTree,
}

impl Default for Document {
    #[inline]
    #[must_use]
    fn default() -> Self {
        let root = ContentNode::new_tree(ContentData::new(
            NativeNodeKind::TransparentContainer.into(),
        ));
        Self::with_content_tree(root.tree())
    }
}

impl Document {
    /// Creates a new empty document.
    ///
    /// # Examples
    ///
    /// ```
    /// use doremi::document::Document;
    ///
    /// let doc = Document::new();
    /// ```
    #[inline]
    #[must_use]
    pub fn new() -> Self {
        Self::default()
    }

    /// Creates a new empty document from the given content tree.
    #[inline]
    #[must_use]
    pub fn with_content_tree(tree: ContentTree) -> Self {
        Self { content: tree }
    }

    /// Returns the content tree.
    #[inline]
    #[must_use]
    pub fn content_tree(&self) -> ContentTree {
        self.content.clone()
    }

    /// Returns the root node of the content tree.
    ///
    /// # Examples
    ///
    /// ```
    /// use doremi::document::Document;
    /// use doremi::node::NativeNodeKind;
    ///
    /// let doc = Document::new();
    /// assert_eq!(
    ///     doc.content_root().borrow_data().kind(),
    ///     NativeNodeKind::TransparentContainer
    /// );
    /// ```
    #[inline]
    #[must_use]
    pub fn content_root(&self) -> ContentNode {
        self.content.root()
    }

    /// Sets the content tree.
    #[inline]
    pub fn set_content_tree(&mut self, tree: ContentTree) {
        self.content = tree;
    }
}
