//! Formatting.

mod common;
pub mod examples;
#[cfg(feature = "markdown")]
#[cfg_attr(docsrs, doc(cfg(feature = "markdown")))]
pub mod markdown;

pub use common::{write_content_tree, SerializeTree};
