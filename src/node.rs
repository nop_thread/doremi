//! Node.

mod kind;

use alloc::borrow::Cow;
use alloc::boxed::Box;

use crate::attribute::Attributes;
use crate::ShortString;

pub use self::kind::{CustomNodeKind, NativeNodeKind, NodeKind, NodeKindId};

/// Document content tree (but not a document itself).
pub type ContentTree = dendron::Tree<ContentData>;

/// Document content node.
pub type ContentNode = dendron::Node<ContentData>;

/// Document content node with hierarchy edit prohibition.
pub type FrozenContentNode = dendron::FrozenNode<ContentData>;

/// Document content node with hierarchy edit grant.
pub type HotContentNode = dendron::HotNode<ContentData>;

/// Content node data.
#[derive(Debug, Clone)]
pub struct ContentData {
    /// Node kind.
    kind: NodeKind,
    /// Attributes.
    attrs: Attributes,
    /// Text fragment (if the node kind is `NativeNodeKind::Text`).
    text: ShortString,
}

impl ContentData {
    /// Creates a new content data.
    #[inline]
    #[must_use]
    pub fn new(kind: NodeKind) -> Self {
        Self {
            kind,
            attrs: Default::default(),
            text: Default::default(),
        }
    }

    /// Creates a new text node data.
    #[cfg(feature = "markdown")]
    #[inline]
    #[must_use]
    pub(crate) fn new_text_from_str(s: &str) -> Self {
        Self {
            kind: NativeNodeKind::Text.into(),
            attrs: Default::default(),
            text: s.into(),
        }
    }

    /// Creates a new text node data.
    #[cfg(feature = "markdown")]
    #[inline]
    #[must_use]
    pub(crate) fn new_text_from_pcmark_cowstr(s: pulldown_cmark::CowStr<'_>) -> Self {
        Self {
            kind: NativeNodeKind::Text.into(),
            attrs: Default::default(),
            text: s.into_string(),
        }
    }

    /// Returns `true` if the node is a text node.
    #[inline]
    #[must_use]
    pub fn is_text(&self) -> bool {
        self.kind == NativeNodeKind::Text
    }

    /// Returns the text fragment data.
    #[inline]
    #[must_use]
    pub fn text(&self) -> Option<&str> {
        if self.is_text() {
            Some(self.text.as_str())
        } else {
            None
        }
    }

    /// Returns the text fragment data.
    ///
    /// # Panics
    ///
    /// Panics if the node kind is not [`NativeNodeKind::Text`].
    #[inline]
    pub fn append_text(&mut self, s: &str) {
        if self.kind != NativeNodeKind::Text {
            panic!(
                "[precondition] text field is not available for non-`Text` node (kind={:?})",
                self.kind
            );
        }
        self.text.push_str(s);
    }

    /// Returns the node kind.
    #[inline]
    #[must_use]
    pub fn kind(&self) -> NodeKind {
        self.kind
    }

    /// Returns the native node kind if available.
    #[inline]
    #[must_use]
    pub fn native_kind(&self) -> Option<NativeNodeKind> {
        match self.kind {
            NodeKind::Native(v) => Some(v),
            NodeKind::Custom(_) => None,
        }
    }

    /// Returns a reference to the attributes.
    #[inline]
    #[must_use]
    pub fn attrs(&self) -> &Attributes {
        &self.attrs
    }

    /// Returns a mutable reference to the attributes.
    #[inline]
    #[must_use]
    pub fn attrs_mut(&mut self) -> &mut Attributes {
        &mut self.attrs
    }
}

impl From<&str> for ContentData {
    #[inline]
    fn from(s: &str) -> ContentData {
        Self {
            kind: NativeNodeKind::Text.into(),
            attrs: Default::default(),
            text: s.into(),
        }
    }
}

impl From<Box<str>> for ContentData {
    #[inline]
    fn from(s: Box<str>) -> ContentData {
        Self {
            kind: NativeNodeKind::Text.into(),
            attrs: Default::default(),
            text: s.into(),
        }
    }
}

impl From<Cow<'_, str>> for ContentData {
    #[inline]
    fn from(s: Cow<'_, str>) -> ContentData {
        Self {
            kind: NativeNodeKind::Text.into(),
            attrs: Default::default(),
            text: s.into(),
        }
    }
}

/// How the node should be rendered.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum RenderType {
    /// Always block.
    Block,
    /// Always inline.
    Inline,
    /// Contextual.
    ///
    /// The actual render type depends on the context and the content.
    /// Processors are responsible to infer or determine the type if necessary.
    Contextual,
    /// Not applicable, or other type (i.e. neither block nor inline).
    NotApplicable,
}
