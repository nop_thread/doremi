//! Footnotes traverseal.

use crate::node::{ContentData, ContentNode, NativeNodeKind};

/// Iterator that iterates footnote definitions.
#[derive(Debug, Clone)]
pub struct FootnoteDefinitionsIter {
    /// Descendants to scan.
    descendants: dendron::traverse::DepthFirstTraverser<ContentData>,
}

impl FootnoteDefinitionsIter {
    /// Creates a new iterator.
    #[must_use]
    #[inline]
    pub fn new(node: &ContentNode) -> Self {
        Self {
            descendants: node.depth_first_traverse(),
        }
    }
}

impl Iterator for FootnoteDefinitionsIter {
    type Item = ContentNode;

    fn next(&mut self) -> Option<Self::Item> {
        let next = self
            .descendants
            .by_ref()
            .filter_map(|node| node.into_open())
            .find(|node| {
                node.borrow_data().native_kind() == Some(NativeNodeKind::FootnoteDefinition)
            })?;
        self.descendants.close_current();
        Some(next)
    }
}
