//! Node attributes.

pub mod native;

use alloc::collections::BTreeMap;

pub use self::native::{NativeAttr, NativeAttrGeneric, NativeAttrKey};

/// Attributes container.
#[derive(Default, Debug, Clone)]
pub struct Attributes {
    /// Native attributes.
    native: BTreeMap<NativeAttrKey, NativeAttrGeneric>,
}

impl Attributes {
    /// Creates a new attributes container.
    #[inline]
    #[must_use]
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns the native attribute.
    #[must_use]
    pub fn native<T: NativeAttr>(&self) -> Option<&T> {
        self.native.get(&T::KEY).map(|v| v.cast_ref())
    }

    /// Returns the native attribute for the key.
    #[inline]
    #[must_use]
    pub fn native_generic(&self, key: NativeAttrKey) -> Option<&NativeAttrGeneric> {
        self.native.get(&key)
    }

    /// Inserts a native attribute.
    #[inline]
    pub fn insert_native<T: Into<NativeAttrGeneric>>(
        &mut self,
        value: T,
    ) -> Option<NativeAttrGeneric> {
        self.insert_native_impl(value.into())
    }

    /// Inserts a native attribute.
    fn insert_native_impl(&mut self, value: NativeAttrGeneric) -> Option<NativeAttrGeneric> {
        let key = value.key();
        self.native.insert(key, value)
    }

    /// Removes a native attribute.
    #[inline]
    pub fn remove_native(&mut self, key: NativeAttrKey) -> Option<NativeAttrGeneric> {
        self.native.remove(&key)
    }

    /// Returns an iterator of the native attributes.
    #[inline]
    #[must_use]
    pub fn iter_native(&self) -> NativeAttrsIter<'_> {
        NativeAttrsIter {
            iter: self.native.values(),
        }
    }
}

/// Iterators for native attributes stored in [`Attributes`].
#[derive(Debug)]
pub struct NativeAttrsIter<'a> {
    /// Inner iterator.
    iter: std::collections::btree_map::Values<'a, NativeAttrKey, NativeAttrGeneric>,
}

impl<'a> Iterator for NativeAttrsIter<'a> {
    type Item = &'a NativeAttrGeneric;

    #[inline]
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next()
    }
}
