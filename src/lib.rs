//! doremi: DOcument REpresentation and Manipulation Interface.
#![forbid(unsafe_code)]
#![warn(rust_2018_idioms)]
// `clippy::missing_docs_in_private_items` implies `missing_docs`.
#![warn(clippy::missing_docs_in_private_items)]
#![warn(clippy::must_use_candidate)]
#![warn(clippy::unwrap_used)]
#![cfg_attr(docsrs, feature(doc_cfg))]

extern crate alloc;

pub mod attribute;
pub mod document;
pub mod format;
pub mod node;
pub mod traverse;

/// A type for a string that is very likely to be short.
// TODO: Use more efficient type.
type ShortString = alloc::string::String;

pub use dendron;
pub use uuid;
