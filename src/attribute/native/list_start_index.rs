//! Start index of an ordered list.

/// Start index of an ordered list.
// CommonMark (v0.30) allows 999999999 ((10^9)-1) as the maximum index, so
// the internal integer type should be able to represent that.
// In other words, use `NonZeroU32`, `u32`, or larger types.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct ListStartIndex(u64);

impl ListStartIndex {
    /// Creates an attribute value.
    #[inline]
    #[must_use]
    pub fn new(index: u64) -> Self {
        Self(index)
    }

    /// Returns the index value.
    #[inline]
    #[must_use]
    pub fn to_u64(self) -> u64 {
        self.0
    }
}
