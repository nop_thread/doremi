//! Raw fragment format.

/// Format of a raw fragment.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum RawFragmentFormat {
    /// Unsupported.
    Unsupported,
    /// HTML.
    Html,
}
