//! Link title.

use alloc::borrow::Cow;
use alloc::boxed::Box;
use alloc::string::String;

use crate::ShortString;

/// Link title.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct LinkTitle(ShortString);

impl LinkTitle {
    /// Returns the title as a string slice.
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl From<String> for LinkTitle {
    #[inline]
    fn from(s: String) -> Self {
        Self(s)
    }
}

impl From<&str> for LinkTitle {
    #[inline]
    fn from(s: &str) -> Self {
        Self(s.into())
    }
}

impl From<Cow<'_, str>> for LinkTitle {
    #[inline]
    fn from(s: Cow<'_, str>) -> Self {
        Self(s.into())
    }
}

impl From<Box<str>> for LinkTitle {
    #[inline]
    fn from(s: Box<str>) -> Self {
        Self(s.into())
    }
}

#[cfg(feature = "markdown")]
impl From<pulldown_cmark::CowStr<'_>> for LinkTitle {
    fn from(s: pulldown_cmark::CowStr<'_>) -> Self {
        Self(s.into_string())
    }
}

impl From<LinkTitle> for String {
    #[inline]
    fn from(id: LinkTitle) -> Self {
        id.0
    }
}

impl From<LinkTitle> for Box<str> {
    #[inline]
    fn from(id: LinkTitle) -> Self {
        id.0.into()
    }
}
