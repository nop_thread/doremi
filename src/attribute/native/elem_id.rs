//! Element ID.

use alloc::borrow::Cow;
use alloc::boxed::Box;
use alloc::string::String;

use crate::ShortString;

/// Element ID.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct ElemId(ShortString);

impl ElemId {
    /// Returns the raw ID as a string slice.
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl From<String> for ElemId {
    #[inline]
    fn from(s: String) -> Self {
        Self(s)
    }
}

impl From<&str> for ElemId {
    #[inline]
    fn from(s: &str) -> Self {
        Self(s.into())
    }
}

impl From<Cow<'_, str>> for ElemId {
    #[inline]
    fn from(s: Cow<'_, str>) -> Self {
        Self(s.into())
    }
}

impl From<Box<str>> for ElemId {
    #[inline]
    fn from(s: Box<str>) -> Self {
        Self(s.into())
    }
}

#[cfg(feature = "markdown")]
impl From<pulldown_cmark::CowStr<'_>> for ElemId {
    fn from(s: pulldown_cmark::CowStr<'_>) -> Self {
        Self(s.into_string())
    }
}

impl From<ElemId> for String {
    #[inline]
    fn from(id: ElemId) -> Self {
        id.0
    }
}

impl From<ElemId> for Box<str> {
    #[inline]
    fn from(id: ElemId) -> Self {
        id.0.into()
    }
}
