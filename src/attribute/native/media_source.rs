//! Media data source URI.

use alloc::borrow::Cow;
use alloc::boxed::Box;
use alloc::string::String;

use crate::ShortString;

/// Media data source URI.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct MediaSource(ShortString);

impl MediaSource {
    /// Returns the media source.
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl From<String> for MediaSource {
    #[inline]
    fn from(s: String) -> Self {
        Self(s)
    }
}

impl From<&str> for MediaSource {
    #[inline]
    fn from(s: &str) -> Self {
        Self(s.into())
    }
}

impl From<Cow<'_, str>> for MediaSource {
    #[inline]
    fn from(s: Cow<'_, str>) -> Self {
        Self(s.into())
    }
}

impl From<Box<str>> for MediaSource {
    #[inline]
    fn from(s: Box<str>) -> Self {
        Self(s.into())
    }
}

#[cfg(feature = "markdown")]
impl From<pulldown_cmark::CowStr<'_>> for MediaSource {
    fn from(s: pulldown_cmark::CowStr<'_>) -> Self {
        Self(s.into_string())
    }
}

impl From<MediaSource> for String {
    #[inline]
    fn from(id: MediaSource) -> Self {
        id.0
    }
}

impl From<MediaSource> for Box<str> {
    #[inline]
    fn from(id: MediaSource) -> Self {
        id.0.into()
    }
}
