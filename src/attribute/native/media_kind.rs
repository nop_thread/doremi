//! Media kind.

/// Media kind.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum MediaKind {
    /// Unspecified or unknown.
    Unknown,
    /// Audio.
    Audio,
    /// Image and (possibly) animation without audio data.
    Image,
    /// Video.
    ///
    /// Video may have optional synchronized audio data.
    Video,
}

impl Default for MediaKind {
    #[inline]
    fn default() -> Self {
        Self::Unknown
    }
}
