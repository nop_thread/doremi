//! Table column specs.

/// Table column specs.
#[derive(Default, Debug, Clone)]
pub struct TableColumnSpecs {
    /// Column specs.
    specs: Vec<TableColumnSpec>,
}

impl TableColumnSpecs {
    /// Creates a new value.
    #[inline]
    #[must_use]
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns the number of column specs.
    #[inline]
    #[must_use]
    pub fn len(&self) -> usize {
        self.specs.len()
    }

    /// Returns true if no column specs are set.
    #[inline]
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.specs.is_empty()
    }

    /// Returns the column specs.
    #[inline]
    #[must_use]
    pub fn specs(&self) -> &[TableColumnSpec] {
        &self.specs
    }

    /// Collects the column specs and set them.
    pub fn set_collected_specs<I>(&mut self, specs: I)
    where
        I: IntoIterator<Item = TableColumnSpec>,
    {
        self.specs = specs.into_iter().collect();
    }

    /// Returns true if the column widths are calculatable.
    ///
    /// When `false` is returned, it indicates there are columns that have
    /// [`TableColumnWidth::Default`] as the width.
    #[must_use]
    pub fn is_widths_calculatable(&self) -> bool {
        self.specs
            .iter()
            .all(|col| !matches!(col.width(), TableColumnWidth::Default))
    }
}

/// Single table column spec.
#[derive(Default, Debug, Clone, Copy)]
pub struct TableColumnSpec {
    /// Column width.
    width: TableColumnWidth,
    /// Horizontal alignment.
    align: TableColumnAlign,
}

impl TableColumnSpec {
    /// Creates a new column spec.
    #[inline]
    #[must_use]
    pub fn new() -> Self {
        Self::default()
    }

    /// Returns the width.
    #[inline]
    #[must_use]
    pub fn width(&self) -> TableColumnWidth {
        self.width
    }

    /// Sets the width to the column.
    ///
    /// # Panics
    ///
    /// Panics if the width is invalid.
    #[inline]
    pub fn set_width(&mut self, width: TableColumnWidth) {
        if !width.is_valid() {
            panic!("[precondition] the table column width must be valid, but got {width:?}");
        }
        self.width = width;
    }

    /// Sets the width to the column and returns the modified value.
    ///
    /// # Panics
    ///
    /// Panics if the width is invalid.
    #[inline]
    #[must_use]
    pub fn and_width(mut self, width: TableColumnWidth) -> Self {
        self.set_width(width);
        self
    }

    /// Returns the alignment.
    #[inline]
    #[must_use]
    pub fn align(&self) -> TableColumnAlign {
        self.align
    }

    /// Sets the alignment.
    #[inline]
    pub fn set_align(&mut self, align: TableColumnAlign) {
        self.align = align;
    }

    /// Sets the alignment and returns the modified value.
    #[inline]
    #[must_use]
    pub fn and_align(mut self, align: TableColumnAlign) -> Self {
        self.set_align(align);
        self
    }
}

/// Table column width.
#[derive(Default, Debug, Clone, Copy)]
pub enum TableColumnWidth {
    /// Unspecified. Processor will choose the default.
    ///
    /// Note that nothing will be guaranteed about the width calculation when
    /// any of the columns has `Default` as the width. It is users' responsibility
    /// to set the appropriate values.
    #[default]
    Default,
    /// Proportion of a column to the total width.
    ///
    /// 0.0 means 0%, 1.0 means 100%.
    ///
    /// If the sum of all percentages are over 1.0 (100%), then they will be
    /// shrinked to 1.0 as total. For example, `[100%, 200%, 100%]` is considered
    /// as `[25%, 50%, 25%]`.
    Ratio(f64),
    /// Stretch.
    ///
    /// If the column widths are `[1.0, 2.0, 2.0]`, then they are calculated to
    /// `[20%, 40%, 40%]`.
    /// If the column widths are `[1.0, 85%, 2.0]`, then they are calculated to
    /// `[5%, 85%, 10%]`.
    Stretch(f64),
}

impl TableColumnWidth {
    /// Returns true if the value is valid width.
    #[must_use]
    pub fn is_valid(self) -> bool {
        match self {
            Self::Default => true,
            // Over 1.0 (100%) would be possible, so do not check the upper bound.
            Self::Ratio(v) => v.is_finite() && v >= 0.0,
            Self::Stretch(v) => v.is_finite() && v >= 0.0,
        }
    }
}

/// Table column alignment.
#[derive(Default, Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TableColumnAlign {
    /// Unspecified. Processor will choose the default.
    #[default]
    Default,
    /// `Left` for LTR context, `Right` for RTL context.
    Start,
    /// `Right` for LTR context, `Left` for RTL context.
    End,
    /// Left.
    Left,
    /// Right.
    Right,
    /// Center.
    Center,
    /// Justify.
    Justify,
}
