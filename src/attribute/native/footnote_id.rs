//! Footnote ID.

use alloc::borrow::Cow;
use alloc::boxed::Box;
use alloc::string::String;

use crate::ShortString;

/// Footnote ID.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub struct FootnoteId(ShortString);

impl FootnoteId {
    /// Returns the raw ID as a string slice.
    #[inline]
    #[must_use]
    pub fn as_str(&self) -> &str {
        self.0.as_str()
    }
}

impl From<String> for FootnoteId {
    #[inline]
    fn from(s: String) -> Self {
        Self(s)
    }
}

impl From<&str> for FootnoteId {
    #[inline]
    fn from(s: &str) -> Self {
        Self(s.into())
    }
}

impl From<Cow<'_, str>> for FootnoteId {
    #[inline]
    fn from(s: Cow<'_, str>) -> Self {
        Self(s.into())
    }
}

impl From<Box<str>> for FootnoteId {
    #[inline]
    fn from(s: Box<str>) -> Self {
        Self(s.into())
    }
}

#[cfg(feature = "markdown")]
impl From<pulldown_cmark::CowStr<'_>> for FootnoteId {
    fn from(s: pulldown_cmark::CowStr<'_>) -> Self {
        Self(s.into_string())
    }
}

impl From<FootnoteId> for String {
    #[inline]
    fn from(id: FootnoteId) -> Self {
        id.0
    }
}

impl From<FootnoteId> for Box<str> {
    #[inline]
    fn from(id: FootnoteId) -> Self {
        id.0.into()
    }
}
