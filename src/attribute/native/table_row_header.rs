//! Table row header.

/// Table row header.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum TableRowHeader {
    /// No row header.
    None,
    /// First column.
    FirstColumn,
}

impl Default for TableRowHeader {
    #[inline]
    fn default() -> Self {
        Self::None
    }
}
