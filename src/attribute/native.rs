//! Natively supported attributes.

mod elem_id;
mod footnote_id;
mod href;
mod link_title;
mod list_start_index;
mod media_kind;
mod media_source;
mod raw_fragment_format;
mod table_column_specs;
mod table_row_header;

pub use self::elem_id::ElemId;
pub use self::footnote_id::FootnoteId;
pub use self::href::Href;
pub use self::link_title::LinkTitle;
pub use self::list_start_index::ListStartIndex;
pub use self::media_kind::MediaKind;
pub use self::media_source::MediaSource;
pub use self::raw_fragment_format::RawFragmentFormat;
pub use self::table_column_specs::{
    TableColumnAlign, TableColumnSpec, TableColumnSpecs, TableColumnWidth,
};
pub use self::table_row_header::TableRowHeader;

/// A trait for dedicated types for natively supported node attributes.
// NOTE: This should be sealed since `NativeAttrGeneric::cast_ref` depends on
// the internal knowledge of the matching keys and the value types.
pub trait NativeAttr: Into<NativeAttrGeneric> + private::Sealed + 'static {
    /// Attribute key.
    const KEY: NativeAttrKey;

    /// Creates a reference to the dedicated type from a reference to the generic type.
    #[must_use]
    fn from_generic_ref(attr: &NativeAttrGeneric) -> Option<&Self>;
}

/// Natively supported node attribute.
#[derive(Debug, Clone)]
#[non_exhaustive]
pub enum NativeAttrGeneric {
    /// Footnote ID.
    FootnoteId(FootnoteId),
    /// Link target IRI.
    Href(Href),
    /// Node node ID.
    Id(ElemId),
    /// Link title.
    LinkTitle(LinkTitle),
    /// Start index of an ordered list.
    ///
    /// Intended to be used with [`NativeNodeKind::ListOrdered`].
    ///
    /// [`NativeNodeKind::ListOrdered`]: `crate::node::NativeNodeKind::ListOrdered`
    ListStartIndex(ListStartIndex),
    /// Media kind.
    MediaKind(MediaKind),
    /// Media source URI.
    MediaSource(MediaSource),
    /// Format of a raw fragment.
    ///
    /// Intended to be used with [`NativeNodeKind::RawFragment`].
    ///
    /// [`NativeNodeKind::RawFragment`]: `crate::node::NativeNodeKind::RawFragment`
    RawFragmentFormat(RawFragmentFormat),
    /// Table column specs.
    ///
    /// Intended to be used with [`NativeNodeKind::Table`].
    ///
    /// [`NativeNodeKind::Table`]: `crate::node::NativeNodeKind::Table`
    TableColumnSpecs(TableColumnSpecs),
    /// Table row header.
    ///
    /// Intended to be used with [`NativeNodeKind::Table`].
    ///
    /// [`NativeNodeKind::Table`]: `crate::node::NativeNodeKind::Table`
    TableRowHeader(TableRowHeader),
}

impl NativeAttrGeneric {
    /// Returns the key for the attribute.
    #[must_use]
    pub fn key(&self) -> NativeAttrKey {
        match self {
            Self::FootnoteId(..) => NativeAttrKey::FootnoteId,
            Self::Href(..) => NativeAttrKey::Href,
            Self::Id(..) => NativeAttrKey::Id,
            Self::LinkTitle(..) => NativeAttrKey::LinkTitle,
            Self::ListStartIndex(..) => NativeAttrKey::ListStartIndex,
            Self::MediaKind(..) => NativeAttrKey::MediaKind,
            Self::MediaSource(..) => NativeAttrKey::MediaSource,
            Self::RawFragmentFormat(..) => NativeAttrKey::RawFragmentFormat,
            Self::TableColumnSpecs(..) => NativeAttrKey::TableColumnSpecs,
            Self::TableRowHeader(..) => NativeAttrKey::TableRowHeader,
        }
    }

    /// Casts the reference to a generic attr into a reference to the dedicated attr.
    #[inline]
    #[must_use]
    pub fn cast_ref<T: NativeAttr>(&self) -> &T {
        T::from_generic_ref(self).expect(
            "[validity] `NativeAttr` is sealed and the type has one-to-one mapping between keys",
        )
    }
}

/// Implement traits necessary for dedicated native attribute types.
macro_rules! setup_native_attr {
    ($variant:ident) => {
        setup_native_attr!($variant, $variant);
    };
    ($variant:ident, $ty:ty) => {
        impl From<$ty> for NativeAttrGeneric {
            #[inline]
            fn from(v: $ty) -> Self {
                Self::$variant(v)
            }
        }

        impl private::Sealed for $ty {}

        impl NativeAttr for $ty {
            const KEY: NativeAttrKey = NativeAttrKey::$variant;

            #[inline]
            fn from_generic_ref(attr: &NativeAttrGeneric) -> Option<&Self> {
                match attr {
                    NativeAttrGeneric::$variant(v) => Some(v),
                    _ => None,
                }
            }
        }
    };
}

setup_native_attr!(FootnoteId);
setup_native_attr!(Href);
setup_native_attr!(Id, ElemId);
setup_native_attr!(LinkTitle);
setup_native_attr!(ListStartIndex);
setup_native_attr!(MediaKind);
setup_native_attr!(MediaSource);
setup_native_attr!(RawFragmentFormat);
setup_native_attr!(TableColumnSpecs);
setup_native_attr!(TableRowHeader);

/// A key for natively supported node attributes.
///
/// `Ord` and `PartialOrd` is provided only for use with `BTreeMap` and similar
/// containers. In other words, the stability of the order of the variants are
/// not guaranteed to be preserved between different versions of this crate.
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord, Hash)]
#[non_exhaustive]
pub enum NativeAttrKey {
    /// Footnote ID.
    FootnoteId,
    /// Link target IRI.
    Href,
    /// Node node ID.
    Id,
    /// Link title.
    LinkTitle,
    /// Start index of an ordered list.
    ///
    /// Intended to be used with [`NativeNodeKind::ListOrdered`].
    ///
    /// [`NativeNodeKind::ListOrdered`]: `crate::node::NativeNodeKind::ListOrdered`
    ListStartIndex,
    /// Media kind.
    MediaKind,
    /// Media source URI.
    MediaSource,
    /// Format of a raw fragment.
    ///
    /// Intended to be used with [`NativeNodeKind::RawFragment`].
    ///
    /// [`NativeNodeKind::RawFragment`]: `crate::node::NativeNodeKind::RawFragment`
    RawFragmentFormat,
    /// Table column specs.
    ///
    /// Intended to be used with [`NativeNodeKind::Table`].
    ///
    /// [`NativeNodeKind::Table`]: `crate::node::NativeNodeKind::Table`
    TableColumnSpecs,
    /// Table row header.
    ///
    /// Intended to be used with [`NativeNodeKind::Table`].
    ///
    /// [`NativeNodeKind::Table`]: `crate::node::NativeNodeKind::Table`
    TableRowHeader,
}

/// Private module for sealed trait.
///
/// See the documentation for [`private::Sealed`] trait.
mod private {
    /// A trait to prohibit downstream to implement some public traits.
    ///
    /// See the [Rust API Guidelines].
    ///
    /// [Rust API Guidelines]:
    ///     https://rust-lang.github.io/api-guidelines/future-proofing.html#sealed-traits-protect-against-downstream-implementations-c-sealed
    pub trait Sealed {}
}
