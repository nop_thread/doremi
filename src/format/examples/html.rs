//! HTML output.

use core::cell::Ref;
use core::fmt;
use core::ops::ControlFlow;

#[cfg(feature = "std")]
use std::io;

use crate::attribute::native::{
    Href, LinkTitle, ListStartIndex, MediaKind, MediaSource, RawFragmentFormat, TableColumnAlign,
    TableColumnSpecs, TableRowHeader,
};
use crate::attribute::NativeAttrGeneric;
use crate::format::SerializeTree;
use crate::node::{ContentData, ContentNode, NativeNodeKind, NodeKind};

#[cfg(feature = "std")]
use crate::format::common::FmtWriterOnIoWrite;

/// Customizable converter from doremi content into HTML.
///
/// The writer is expected to implement [`core::fmt::Write`].
///
/// # Examples
///
/// ```
/// use doremi::dendron::tree;
/// use doremi::format::write_content_tree;
/// use doremi::format::examples::html::HtmlFmtWriter;
/// use doremi::node::{ContentData, NativeNodeKind};
///
/// let tree = tree! {
///     ContentData::new(NativeNodeKind::EmphasisStrong.into()), [
///         ContentData::from("Important!")
///     ],
/// };
///
/// let formatter = write_content_tree(HtmlFmtWriter::new(String::new()), &tree)
///     .expect("failed to serialize");
/// let converted = formatter.finish();
///
/// assert_eq!(converted, "<strong>Important!</strong>");
/// ```
#[derive(Default)]
pub struct HtmlFmtWriter<W> {
    /// Writer.
    writer: W,
    /// Heading level offset.
    heading_level_offset: isize,
}

impl<W: fmt::Write> HtmlFmtWriter<W> {
    /// Creates a new writer.
    #[inline]
    #[must_use]
    pub fn new(writer: W) -> Self {
        Self {
            writer,
            heading_level_offset: 0,
        }
    }

    /// Creates a new writer with the given heading level offset.
    #[inline]
    #[must_use]
    pub fn with_heading_level_offset(writer: W, heading_level_offset: isize) -> Self {
        Self {
            writer,
            heading_level_offset,
        }
    }

    /// Returns the internal writer and the I/O error if available.
    #[inline]
    #[must_use]
    pub fn finish(self) -> W {
        self.writer
    }
}

impl<W: fmt::Write> SerializeTree for HtmlFmtWriter<W> {
    type Error = fmt::Error;

    #[inline]
    fn write_fmt(&mut self, fmt: fmt::Arguments<'_>) -> Result<(), Self::Error> {
        self.writer.write_fmt(fmt)
    }

    fn process_event(&mut self, node: &ContentNode, is_open: bool) -> Result<bool, Self::Error> {
        let native_elem_kind = match node.borrow_data().native_kind() {
            Some(v) => v,
            None => {
                assert!(is_open);
                let kind = match node.borrow_data().kind() {
                    NodeKind::Native(_) => {
                        unreachable!("[consistency] native node kinds are already processed")
                    }
                    NodeKind::Custom(v) => v,
                };
                write!(
                    self.writer,
                    "<span class='doremi-error'><!-- {{{{{kind:?}}}}} --></span>"
                )?;
                return Ok(true);
            }
        };
        match (native_elem_kind, is_open) {
            (NativeNodeKind::BlockTitle, _) => {
                match node
                    .parent()
                    .and_then(|parent| parent.borrow_data().native_kind())
                {
                    Some(kind) if kind.is_sectioning_content_or_root() => {
                        let level = section_level(node);
                        // Just doing "saturating usize + isize".
                        let level = if self.heading_level_offset < 0 {
                            level.saturating_sub(self.heading_level_offset.wrapping_abs() as usize)
                        } else {
                            level.saturating_add(self.heading_level_offset as usize)
                        };
                        let level = level.clamp(1, 6);
                        if is_open {
                            write!(self.writer, "<h{level}")?;
                            write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                            write!(self.writer, ">")?;
                        } else {
                            writeln!(self.writer, "</h{level}>")?;
                        }
                    }
                    _ => {
                        if is_open {
                            write!(self.writer, "<div")?;
                            write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                            write!(self.writer, ">")?;
                        } else {
                            writeln!(self.writer, "</div>")?;
                        }
                    }
                }
            }
            (NativeNodeKind::CodeBlock, true) => {
                write!(self.writer, "<pre")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, "><code>")?;
            }
            (NativeNodeKind::CodeBlock, false) => write!(self.writer, "</code></pre>")?,
            (NativeNodeKind::CodeInline, true) => {
                write!(self.writer, "<code")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::CodeInline, false) => write!(self.writer, "</code>")?,
            (NativeNodeKind::EmphasisStress, true) => {
                write!(self.writer, "<em")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::EmphasisStress, false) => write!(self.writer, "</em>")?,
            (NativeNodeKind::EmphasisStrong, true) => {
                write!(self.writer, "<strong")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::EmphasisStrong, false) => write!(self.writer, "</strong>")?,
            (NativeNodeKind::FootnoteDefinition, _) => {
                assert!(is_open);
                // Users should handle footnote definitions manually.
                // This function does not write them.
                return Ok(true);
            }
            (NativeNodeKind::FootnoteReference, true) => {
                write!(self.writer, "<sup")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, "><a")?;
                let attrs = Ref::map(node.borrow_data(), ContentData::attrs);
                if let Some(href) = attrs.native::<Href>() {
                    write!(self.writer, " href=\"{}\"", EscapedAttr(href.as_str()))?;
                }
                write!(self.writer, ">[")?;
            }
            (NativeNodeKind::FootnoteReference, false) => write!(self.writer, "]</a></sup>")?,
            (NativeNodeKind::ForcedLineBreak, _) => {
                assert!(is_open);
                write!(self.writer, "<br")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, "/>")?;
                return Ok(true);
            }
            (NativeNodeKind::GenericBlock, true) => {
                write!(self.writer, "<div")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::GenericBlock, false) => writeln!(self.writer, "</div>")?,
            (NativeNodeKind::GenericInline, true) => {
                write!(self.writer, "<span")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::GenericInline, false) => write!(self.writer, "</span>")?,
            (NativeNodeKind::Link, true) => {
                write!(self.writer, "<a")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                let attrs = Ref::map(node.borrow_data(), ContentData::attrs);
                if let Some(href) = attrs.native::<Href>() {
                    write!(self.writer, " href=\"{}\"", EscapedAttr(href.as_str()))?;
                }
                if let Some(title) = attrs.native::<LinkTitle>() {
                    write!(self.writer, " title=\"{}\"", EscapedAttr(title.as_str()))?;
                }
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::Link, false) => write!(self.writer, "</a>")?,
            (NativeNodeKind::ListItem, true) => {
                write!(self.writer, "<li")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::ListItem, false) => write!(self.writer, "</li>")?,
            (NativeNodeKind::ListOrdered, true) => {
                write!(self.writer, "<ol")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                let attrs = Ref::map(node.borrow_data(), ContentData::attrs);
                if let Some(start) = attrs
                    .native::<ListStartIndex>()
                    .map(|v| v.to_u64())
                    .filter(|v| *v != 1)
                {
                    write!(self.writer, " start=\"{}\"", start)?;
                }
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::ListOrdered, false) => writeln!(self.writer, "</ol>")?,
            (NativeNodeKind::ListUnordered, true) => {
                write!(self.writer, "<ul")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::ListUnordered, false) => writeln!(self.writer, "</ul>")?,
            (NativeNodeKind::MediaBlock | NativeNodeKind::MediaInline, _) => {
                let attrs = Ref::map(node.borrow_data(), ContentData::attrs);
                let media_kind = attrs.native::<MediaKind>().copied().unwrap_or_default();
                match media_kind {
                    MediaKind::Unknown => {
                        write!(self.writer, "<object")?;
                        if let Some(source) = attrs.native::<MediaSource>() {
                            write!(self.writer, " data=\"{}\"", EscapedAttr(source.as_str()))?;
                        }
                        write!(self.writer, "></object>")?;
                        return Ok(true);
                    }
                    MediaKind::Audio => {
                        if is_open {
                            write!(self.writer, "<audio controls=\"controls\"")?;
                            if let Some(source) = attrs.native::<MediaSource>() {
                                write!(self.writer, " src=\"{}\"", EscapedAttr(source.as_str()))?;
                            }
                            if let Some(title) = attrs.native::<LinkTitle>() {
                                write!(self.writer, " title=\"{}\"", EscapedAttr(title.as_str()))?;
                            }
                            write!(self.writer, ">")?;
                        } else {
                            write!(self.writer, "</audio>")?;
                        }
                    }
                    MediaKind::Image => {
                        write!(self.writer, "<img")?;
                        if let Some(source) = attrs.native::<MediaSource>() {
                            write!(self.writer, " src=\"{}\"", EscapedAttr(source.as_str()))?;
                        }
                        // TODO: Set `alt` attribute.
                        write!(self.writer, "/>")?;
                        return Ok(true);
                    }
                    MediaKind::Video => {
                        if is_open {
                            write!(self.writer, "<video controls=\"controls\"")?;
                            if let Some(source) = attrs.native::<MediaSource>() {
                                write!(self.writer, " src=\"{}\"", EscapedAttr(source.as_str()))?;
                            }
                            write!(self.writer, ">")?;
                        } else {
                            write!(self.writer, "</video>")?;
                        }
                    }
                }
            }
            (NativeNodeKind::Paragraph, true) => {
                write!(self.writer, "<p")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::Paragraph, false) => writeln!(self.writer, "</p>")?,
            (NativeNodeKind::QuoteBlock, true) => {
                write!(self.writer, "<blockquote")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::QuoteBlock, false) => writeln!(self.writer, "</blockquote>")?,
            (NativeNodeKind::RawFragment, _) => {
                let format = node
                    .borrow_data()
                    .attrs()
                    .native::<RawFragmentFormat>()
                    .cloned();
                if format != Some(RawFragmentFormat::Html) {
                    write!(
                        self.writer,
                        "<span class='doremi-error'><!-- {{{{non-HTML raw fragment}}}} --></span>"
                    )?;
                    return Ok(true);
                }
            }
            (NativeNodeKind::Section, true) => {
                write!(self.writer, "<section")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::Section, false) => writeln!(self.writer, "</section>")?,
            (NativeNodeKind::Table, true) => {
                write!(self.writer, "<table")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;

                let colspecs = node
                    .borrow_data()
                    .attrs()
                    .native::<TableColumnSpecs>()
                    .cloned();
                if let Some(colspecs) = colspecs {
                    write!(self.writer, "<colgroup>")?;
                    // TODO: Calculate column widths.
                    for col in colspecs.specs() {
                        let align_css = match col.align() {
                            TableColumnAlign::Default => None,
                            TableColumnAlign::Start => Some("start"),
                            TableColumnAlign::End => Some("end"),
                            TableColumnAlign::Left => Some("left"),
                            TableColumnAlign::Right => Some("right"),
                            TableColumnAlign::Center => Some("center"),
                            TableColumnAlign::Justify => Some("justify"),
                        };
                        write!(self.writer, "<col style='")?;
                        if let Some(align) = align_css {
                            write!(self.writer, "text-align: {align};")?;
                        }
                        write!(self.writer, "'/>")?;
                    }
                    write!(self.writer, "</colgroup>")?;
                }
            }
            (NativeNodeKind::Table, false) => writeln!(self.writer, "</table>")?,
            (NativeNodeKind::TableBody, true) => {
                write!(self.writer, "<tbody")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::TableBody, false) => writeln!(self.writer, "</tbody>")?,
            (NativeNodeKind::TableEntry, _) => {
                let num_preceding_entries = node
                    .preceding_siblings_reverse()
                    .filter(|sib| {
                        sib.borrow_data().native_kind() == Some(NativeNodeKind::TableEntry)
                    })
                    .count();
                let tag = if num_preceding_entries == 0 {
                    // The first entry in the row.
                    let table = node.ancestors().find(|ancestor| {
                        ancestor.borrow_data().native_kind() == Some(NativeNodeKind::Table)
                    });
                    let row_header = table
                        .and_then(|table| {
                            table
                                .borrow_data()
                                .attrs()
                                .native::<TableRowHeader>()
                                .copied()
                        })
                        .unwrap_or_default();
                    match row_header {
                        TableRowHeader::None => "td",
                        TableRowHeader::FirstColumn => "th",
                    }
                } else {
                    "td"
                };
                if !is_open {
                    write!(self.writer, "</{tag}>")?;
                    return Ok(false);
                }
                write!(self.writer, "<{tag}")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::TableFooter, true) => {
                write!(self.writer, "<tfoot")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::TableFooter, false) => writeln!(self.writer, "</tfoot>")?,
            (NativeNodeKind::TableHeader, true) => {
                write!(self.writer, "<thead")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::TableHeader, false) => writeln!(self.writer, "</thead>")?,
            (NativeNodeKind::TableRow, true) => {
                write!(self.writer, "<tr")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, ">")?;
            }
            (NativeNodeKind::TableRow, false) => writeln!(self.writer, "</tr>")?,
            (NativeNodeKind::Text, _) => {
                assert!(is_open);
                let text = Ref::map(node.borrow_data(), |data| {
                    data.text()
                        .expect("[validity] text should be available for `Text` node")
                });
                if is_inside_raw_html_fragment(node) {
                    // Do not escape since the text is the raw HTML fragment.
                    write!(self.writer, "{}", &text)?;
                } else {
                    write!(self.writer, "{}", EscapedText(&text))?;
                }
                // Ignore children.
                return Ok(true);
            }
            (NativeNodeKind::ThematicBreak, true) => {
                assert!(is_open);
                write!(self.writer, "<hr")?;
                write_all_global_attrs(&mut self.writer, &node.borrow_data())?;
                write!(self.writer, "/>")?;
                return Ok(true);
            }
            (NativeNodeKind::ThematicBreak, _) => {}
            (NativeNodeKind::TransparentContainer, _) => {}
            (NativeNodeKind::Unprocessable, _) => {
                assert!(is_open);
                write!(
                    self.writer,
                    "<span class='doremi-error'><!-- {{{{unprocessable}}}} --></span>"
                )?;
                return Ok(true);
            }
        }

        Ok(false)
    }
}

/// Customizable converter from doremi content into HTML.
///
/// The writer is expected to implement [`std::io::Write`].
///
/// # Examples
///
/// ```
/// use std::io::Cursor;
/// use doremi::dendron::tree;
/// use doremi::format::write_content_tree;
/// use doremi::format::examples::html::HtmlIoWriter;
/// use doremi::node::{ContentData, NativeNodeKind};
///
/// let tree = tree! {
///     ContentData::new(NativeNodeKind::EmphasisStrong.into()), [
///         ContentData::from("Important!")
///     ],
/// };
///
/// let writer = write_content_tree(HtmlIoWriter::new(Cursor::new(vec![])), &tree)
///     .expect("failed to serialize or write");
/// let converted = writer.finish().0.into_inner();
///
/// assert_eq!(converted, b"<strong>Important!</strong>");
/// ```
#[cfg(feature = "std")]
#[cfg_attr(docsrs, doc(cfg(feature = "std")))]
#[derive(Default)]
pub struct HtmlIoWriter<W>(HtmlFmtWriter<FmtWriterOnIoWrite<W>>);

#[cfg(feature = "std")]
impl<W: io::Write> HtmlIoWriter<W> {
    /// Creates a new writer.
    #[inline]
    #[must_use]
    pub fn new(writer: W) -> Self {
        Self(HtmlFmtWriter::new(FmtWriterOnIoWrite::new(writer)))
    }

    /// Creates a new writer with the given heading level offset.
    #[inline]
    #[must_use]
    pub fn with_heading_level_offset(writer: W, heading_level_offset: isize) -> Self {
        Self(HtmlFmtWriter::with_heading_level_offset(
            FmtWriterOnIoWrite::new(writer),
            heading_level_offset,
        ))
    }

    /// Returns the internal writer and the I/O error if available.
    #[inline]
    #[must_use]
    pub fn finish(self) -> (W, Option<io::Error>) {
        self.0.writer.decompose()
    }
}

#[cfg(feature = "std")]
impl<W: io::Write> SerializeTree for HtmlIoWriter<W> {
    type Error = io::Error;

    #[inline]
    fn write_fmt(&mut self, fmt: fmt::Arguments<'_>) -> Result<(), Self::Error> {
        let _ = self.0.write_fmt(fmt);
        self.0.writer.take_error_as_result()
    }

    fn process_event(&mut self, node: &ContentNode, is_open: bool) -> Result<bool, Self::Error> {
        match self.0.process_event(node, is_open) {
            Ok(v) => Ok(v),
            Err(_) => self
                .0
                .writer
                .take_error_as_result()
                .map(|_| unreachable!("[consistency] I/O error happend")),
        }
    }
}

/// Escaped string for a text node.
///
/// # Examples
///
/// ```
/// use doremi::format::examples::html::EscapedText;
///
/// let src = "<hr> is a tag for \"horizontal rule\".";
/// let escaped = EscapedText::new(src).to_string();
/// assert_eq!(escaped, "&lt;hr&gt; is a tag for \"horizontal rule\".");
/// ```
#[derive(Debug, Clone, Copy)]
pub struct EscapedText<'a>(&'a str);

impl<'a> EscapedText<'a> {
    /// Creates a new escaped text.
    #[inline]
    #[must_use]
    pub fn new(s: &'a str) -> Self {
        Self(s)
    }

    /// Returns the source (non-escaped) text.
    #[inline]
    #[must_use]
    pub fn raw(&self) -> &'a str {
        self.0
    }
}

impl fmt::Display for EscapedText<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut rest = self.0;
        while !rest.is_empty() {
            match rest.bytes().position(|b| [b'<', b'>', b'&'].contains(&b)) {
                Some(pos) => {
                    let (prefix, suffix) = &rest.split_at(pos);
                    f.write_str(prefix)?;
                    let escaped = match suffix.as_bytes()[0] {
                        b'<' => "&lt;",
                        b'>' => "&gt;",
                        b'&' => "&amp;",
                        _ => unreachable!("[consistency] no other characters needs escaping"),
                    };
                    f.write_str(escaped)?;
                    rest = &suffix[1..];
                }
                None => return f.write_str(rest),
            }
        }
        Ok(())
    }
}

/// Writes the attribute if it is global (i.e. meaningful for any node kind).
///
/// Returns `Ok(true)` if the attribute is written. Returns `Ok(false)` if the
/// attribute is not written.
fn write_if_global_attr<W: fmt::Write>(
    writer: &mut W,
    native_attr: &NativeAttrGeneric,
) -> Result<bool, fmt::Error> {
    match native_attr {
        NativeAttrGeneric::Id(id) => write!(writer, " id=\"{}\"", EscapedAttr(id.as_str()))?,
        _ => return Ok(false),
    }
    Ok(true)
}

/// Writes global attributes.
fn write_all_global_attrs<W: fmt::Write>(writer: &mut W, node_data: &ContentData) -> fmt::Result {
    node_data
        .attrs()
        .iter_native()
        .try_for_each(move |native_attr| write_if_global_attr(writer, native_attr).map(|_| ()))
}

/// Escaped string for an attribute value.
///
/// # Examples
///
/// ```
/// use doremi::format::examples::html::EscapedAttr;
///
/// let src = "<hr> is a tag for \"horizontal rule\".";
/// let escaped = EscapedAttr::new(src).to_string();
/// assert_eq!(escaped, "&lt;hr&gt; is a tag for &quot;horizontal rule&quot;.");
/// ```
#[derive(Debug, Clone, Copy)]
pub struct EscapedAttr<'a>(&'a str);

impl<'a> EscapedAttr<'a> {
    /// Creates a new escaped attribute value.
    #[inline]
    #[must_use]
    pub fn new(s: &'a str) -> Self {
        Self(s)
    }

    /// Returns the source (non-escaped) attribute value.
    #[inline]
    #[must_use]
    pub fn raw(&self) -> &'a str {
        self.0
    }
}

impl fmt::Display for EscapedAttr<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut rest = self.0;
        while !rest.is_empty() {
            match rest
                .bytes()
                .position(|b| [b'\'', b'"', b'<', b'>', b'&'].contains(&b))
            {
                Some(pos) => {
                    let (prefix, suffix) = &rest.split_at(pos);
                    f.write_str(prefix)?;
                    let escaped = match suffix.as_bytes()[0] {
                        b'\'' => "&apos;",
                        b'"' => "&quot;",
                        b'<' => "&lt;",
                        b'>' => "&gt;",
                        b'&' => "&amp;",
                        _ => unreachable!("[consistency] no other characters needs escaping"),
                    };
                    f.write_str(escaped)?;
                    rest = &suffix[1..];
                }
                None => return f.write_str(rest),
            }
        }
        Ok(())
    }
}

/// Returns `true` if the node with the given kind is a sectioning root.
#[inline]
#[must_use]
fn is_html_sectioning_root(kind: NativeNodeKind) -> bool {
    // Intentionally not using `matches!()` or `_` wildcard, in order to
    // prevent the condition from being outdated when new kinds are added.
    match kind {
        NativeNodeKind::QuoteBlock => true,
        NativeNodeKind::BlockTitle
        | NativeNodeKind::CodeBlock
        | NativeNodeKind::CodeInline
        | NativeNodeKind::EmphasisStress
        | NativeNodeKind::EmphasisStrong
        | NativeNodeKind::FootnoteDefinition
        | NativeNodeKind::FootnoteReference
        | NativeNodeKind::ForcedLineBreak
        | NativeNodeKind::GenericBlock
        | NativeNodeKind::GenericInline
        | NativeNodeKind::Link
        | NativeNodeKind::ListItem
        | NativeNodeKind::ListOrdered
        | NativeNodeKind::ListUnordered
        | NativeNodeKind::MediaBlock
        | NativeNodeKind::MediaInline
        | NativeNodeKind::Paragraph
        | NativeNodeKind::RawFragment
        | NativeNodeKind::Section
        | NativeNodeKind::Table
        | NativeNodeKind::TableBody
        | NativeNodeKind::TableEntry
        | NativeNodeKind::TableFooter
        | NativeNodeKind::TableHeader
        | NativeNodeKind::TableRow
        | NativeNodeKind::Text
        | NativeNodeKind::ThematicBreak
        | NativeNodeKind::TransparentContainer
        | NativeNodeKind::Unprocessable => false,
    }
}

/// Returns `true` if the node is inside raw HTML fragment.
///
/// This does not check whether the node itself is raw HTML fragment or not.
#[must_use]
fn is_inside_raw_html_fragment(node: &ContentNode) -> bool {
    node.ancestors().any(|ancestor| {
        let data = ancestor.borrow_data();
        data.kind() == NativeNodeKind::RawFragment
            && data.attrs().native::<RawFragmentFormat>() == Some(&RawFragmentFormat::Html)
    })
}

/// Returns the section level of the nearest sectioning node (including the given node).
///
/// Returns the number of sectioning nodes between the given node (inclusive)
/// and the sectioning root node (inclusive).
#[must_use]
fn section_level(node: &ContentNode) -> usize {
    // I want `Iterator::take_while_inclusive` but it is not available as of
    // Rust 1.61. See <https://github.com/rust-lang/rust/issues/62208>.
    let depth = node
        .ancestors_or_self()
        .filter_map(|node| node.borrow_data().native_kind())
        .filter(|kind| kind.is_sectioning_content_or_root())
        .try_fold(0_usize, |depth, kind| {
            let depth = depth + 1;
            if is_html_sectioning_root(kind) {
                ControlFlow::Break(depth)
            } else {
                ControlFlow::Continue(depth)
            }
        });
    match depth {
        ControlFlow::Break(v) | ControlFlow::Continue(v) => v,
    }
}
