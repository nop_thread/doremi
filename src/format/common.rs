//! Common stuff.

#[cfg(feature = "std")]
use core::fmt;
use core::fmt::Arguments;

#[cfg(feature = "std")]
use std::io;

use dendron::traverse::DftEvent;

use crate::node::{ContentNode, ContentTree};

/// A trait for content tree serializer.
pub trait SerializeTree {
    /// Write error or process error.
    type Error;

    /// Writes the formatted string.
    fn write_fmt(&mut self, fmt: Arguments<'_>) -> Result<(), Self::Error>;
    /// Processes the node open/close event.
    ///
    /// Returns whether the current node should be skipped.
    fn process_event(&mut self, node: &ContentNode, is_open: bool) -> Result<bool, Self::Error>;
}

impl<T: SerializeTree> SerializeTree for &mut T {
    type Error = T::Error;

    #[inline]
    fn write_fmt(&mut self, fmt: Arguments<'_>) -> Result<(), Self::Error> {
        (**self).write_fmt(fmt)
    }

    #[inline]
    fn process_event(&mut self, node: &ContentNode, is_open: bool) -> Result<bool, Self::Error> {
        (**self).process_event(node, is_open)
    }
}

/// Adapter to implement `core::fmt::Write` for a type that implements `std::io::Write`.
#[cfg(feature = "std")]
#[derive(Default)]
pub(super) struct FmtWriterOnIoWrite<W> {
    /// `std::io::Write` writer.
    io_writer: W,
    /// I/O error.
    error: Option<io::Error>,
}

#[cfg(feature = "std")]
impl<W: io::Write> FmtWriterOnIoWrite<W> {
    /// Creates a new adapter.
    #[inline]
    #[must_use]
    pub(super) fn new(io_writer: W) -> Self {
        Self {
            io_writer,
            error: None,
        }
    }

    /// Decomposes the adapter.
    #[inline]
    #[must_use]
    pub(super) fn decompose(self) -> (W, Option<io::Error>) {
        let Self { io_writer, error } = self;
        (io_writer, error)
    }

    /// Takes the error from `self` and returns it as `Err(_)` if available.
    #[inline]
    pub(super) fn take_error_as_result(&mut self) -> Result<(), io::Error> {
        match self.error.take() {
            Some(e) => Err(e),
            None => Ok(()),
        }
    }
}

#[cfg(feature = "std")]
impl<W: io::Write> fmt::Write for FmtWriterOnIoWrite<W> {
    fn write_str(&mut self, s: &str) -> Result<(), fmt::Error> {
        if let Err(e) = self.io_writer.write_all(s.as_bytes()) {
            self.error = Some(e);
        }
        Ok(())
    }

    fn write_fmt(&mut self, args: fmt::Arguments<'_>) -> Result<(), fmt::Error> {
        if let Err(e) = self.io_writer.write_fmt(args) {
            self.error = Some(e);
        }
        Ok(())
    }
}

/// Writes a content tree as HTML.
pub fn write_content_tree<S: SerializeTree>(
    mut processor: S,
    tree: &ContentTree,
) -> Result<S, S::Error> {
    let mut traverser = tree.root().depth_first_traverse();

    while let Some(ev) = traverser.next() {
        let (node, is_open) = match ev {
            DftEvent::Open(node) => (node, true),
            DftEvent::Close(node) => (node, false),
        };
        if processor.process_event(&node, is_open)? {
            traverser.close_current();
        }
    }

    Ok(processor)
}
