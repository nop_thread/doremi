//! Example implementations.
//!
//! Note that output of serializers in this module is not guaranteed to be stable.
//! If you want stable output, you should implement your own serializers.

pub mod html;
