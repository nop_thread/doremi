//! Markdown input/output.

use crate::attribute::native::{
    ElemId, FootnoteId, Href, LinkTitle, ListStartIndex, MediaKind, MediaSource, RawFragmentFormat,
    TableColumnAlign, TableColumnSpec, TableColumnSpecs,
};
use crate::node::{
    ContentData, ContentNode, ContentTree, HotContentNode, NativeNodeKind, RenderType,
};

use pulldown_cmark::{self as pcmark, Event, Tag};

/// Reads a markdown document from the string as block elements.
#[must_use]
pub fn read_as_block_content(s: &str, all_extensions: bool) -> ContentTree {
    let parser_opts = if all_extensions {
        pcmark::Options::all()
    } else {
        pcmark::Options::empty()
    };
    let parser = pcmark::Parser::new_ext(s, parser_opts);
    let body_root = HotContentNode::new_tree(ContentData::new(
        NativeNodeKind::TransparentContainer.into(),
    ));
    let mut open_elems: Vec<HotContentNode> = vec![body_root.clone()];
    // `Option<pcmark::HeadingLevel>`: `None` if the element is a sectioning root.
    // If `Option<pcmark::HeadingLevel>` field is zero, it indicates that the parent section is
    // implicitly created without heading. Such sections cannot be closed by the following heading,
    // and should be closed by itself. Currently, only sectioning roots can open such sections.
    let mut open_sections: Vec<(Option<pcmark::HeadingLevel>, HotContentNode)> = Vec::new();
    for ev in parser {
        debug_assert!(
            !open_elems.is_empty(),
            "[consistency] `open_elems` should contain the body root"
        );
        let current_parent = open_elems
            .last()
            .expect("[consistency] `open_elems` should contain the body root");
        match ev {
            Event::Start(tag) => match tag {
                Tag::Paragraph => {
                    let new = current_parent
                        .create_as_last_child(ContentData::new(NativeNodeKind::Paragraph.into()));
                    open_elems.push(new);
                }
                Tag::Heading(raw_level, id, _classes) => {
                    // Close the currently open sections if necessary.
                    {
                        let parent_raw_level = open_sections.last().and_then(|sect| sect.0);
                        // If `parent_raw_level` is `None`, it indicates the parent section is a
                        // sectioning root. In such case, no need to close sections.
                        let num_to_close = parent_raw_level.map_or(0, |plevel| {
                            usize::from(plevel as u8)
                                .saturating_sub(usize::from(raw_level as u8) - 1)
                        });
                        'close_sections: for _ in 0..num_to_close {
                            while let Some(popped_elem) = open_elems.pop() {
                                if open_elems.is_empty() {
                                    // The body root should be kept at the bottom of `open_elems`.
                                    // Push it back.
                                    open_elems.push(popped_elem);
                                    assert!(
                                        open_sections.is_empty(),
                                        "[consistency] body root is not managed by `open_sections`"
                                    );
                                    break 'close_sections;
                                }
                                let kind = popped_elem.borrow_data().native_kind();
                                if kind.map_or(false, NativeNodeKind::is_sectioning_content_or_root)
                                {
                                    // Found a sectioning element.
                                    break;
                                }
                            }
                            if let Some((None, closed)) = open_sections.pop() {
                                // Don't close sections not opened by heading.
                                // Push it back to the stack.
                                open_sections.push((None, closed));
                                break 'close_sections;
                            }
                        }
                    }
                    let section = {
                        let current_parent = open_elems
                            .last()
                            .expect("[consistency] `open_elems` should contain the body root");
                        let mut section_data = ContentData::new(NativeNodeKind::Section.into());
                        if let Some(id) = id {
                            section_data.attrs_mut().insert_native(ElemId::from(id));
                        }
                        // TODO: Set classes attributes.
                        let section = current_parent.create_as_last_child(section_data);
                        open_elems.push(section.clone());
                        open_sections.push((Some(raw_level), section.clone()));
                        section
                    };
                    // Set up heading.
                    let title = section
                        .create_as_last_child(ContentData::new(NativeNodeKind::BlockTitle.into()));
                    open_elems.push(title.clone());
                }
                Tag::BlockQuote => {
                    let new = current_parent
                        .create_as_last_child(ContentData::new(NativeNodeKind::QuoteBlock.into()));
                    open_elems.push(new.clone());
                    open_sections.push((None, new));
                }
                Tag::CodeBlock(_block_kind) => {
                    // TODO: Set attributes.
                    let new = current_parent
                        .create_as_last_child(ContentData::new(NativeNodeKind::CodeBlock.into()));
                    open_elems.push(new);
                }
                Tag::List(None) => {
                    let new = current_parent.create_as_last_child(ContentData::new(
                        NativeNodeKind::ListUnordered.into(),
                    ));
                    open_elems.push(new);
                }
                Tag::List(Some(first_index)) => {
                    let mut data = ContentData::new(NativeNodeKind::ListOrdered.into());
                    data.attrs_mut()
                        .insert_native(ListStartIndex::new(first_index));
                    let new = current_parent.create_as_last_child(data);
                    open_elems.push(new);
                }
                Tag::Item => {
                    let new = current_parent
                        .create_as_last_child(ContentData::new(NativeNodeKind::ListItem.into()));
                    open_elems.push(new);
                }
                Tag::FootnoteDefinition(label) => {
                    let mut data = ContentData::new(NativeNodeKind::FootnoteDefinition.into());
                    data.attrs_mut().insert_native(FootnoteId::from(label));
                    let new = current_parent.create_as_last_child(data);
                    open_elems.push(new);
                }
                Tag::Table(col_aligns) => {
                    let mut data = ContentData::new(NativeNodeKind::Table.into());
                    let mut colspecs = TableColumnSpecs::new();
                    colspecs.set_collected_specs(col_aligns.into_iter().map(|align| {
                        let align = match align {
                            pcmark::Alignment::None => TableColumnAlign::Default,
                            pcmark::Alignment::Left => TableColumnAlign::Left,
                            pcmark::Alignment::Center => TableColumnAlign::Center,
                            pcmark::Alignment::Right => TableColumnAlign::Right,
                        };
                        TableColumnSpec::new().and_align(align)
                    }));
                    data.attrs_mut().insert_native(colspecs);
                    let new = current_parent.create_as_last_child(data);
                    open_elems.push(new);
                }
                Tag::TableHead => {
                    let new_header = current_parent
                        .create_as_last_child(ContentData::new(NativeNodeKind::TableHeader.into()));
                    let new_row = new_header
                        .create_as_last_child(ContentData::new(NativeNodeKind::TableRow.into()));
                    open_elems.push(new_header);
                    open_elems.push(new_row);
                }
                Tag::TableRow => {
                    let row_parent = if current_parent.borrow_data().native_kind()
                        != Some(NativeNodeKind::TableBody)
                    {
                        let new_tbody = current_parent.create_as_last_child(ContentData::new(
                            NativeNodeKind::TableBody.into(),
                        ));
                        open_elems.push(new_tbody);
                        open_elems
                            .last()
                            .expect("[validity] not empty since just pushed a value")
                    } else {
                        current_parent
                    };
                    let new = row_parent
                        .create_as_last_child(ContentData::new(NativeNodeKind::TableRow.into()));
                    open_elems.push(new);
                }
                Tag::TableCell => {
                    // TODO: Apply attributes.
                    let new = current_parent
                        .create_as_last_child(ContentData::new(NativeNodeKind::TableEntry.into()));
                    open_elems.push(new);
                }
                Tag::Emphasis => {
                    let new = current_parent.create_as_last_child(ContentData::new(
                        NativeNodeKind::EmphasisStress.into(),
                    ));
                    open_elems.push(new);
                }
                Tag::Strong => {
                    let new = current_parent.create_as_last_child(ContentData::new(
                        NativeNodeKind::EmphasisStrong.into(),
                    ));
                    open_elems.push(new);
                }
                Tag::Strikethrough => {
                    // TODO: Add a new node kind.
                    let new = current_parent.create_as_last_child(ContentData::new(
                        NativeNodeKind::GenericInline.into(),
                    ));
                    open_elems.push(new);
                }
                Tag::Link(_link_type, dest_uri, title) => {
                    let mut data = ContentData::new(NativeNodeKind::Link.into());
                    data.attrs_mut().insert_native(Href::from(dest_uri));
                    if !title.is_empty() {
                        data.attrs_mut().insert_native(LinkTitle::from(title));
                    }
                    let new = current_parent.create_as_last_child(data);
                    open_elems.push(new);
                }
                Tag::Image(_link_type, dest_uri, title) => {
                    let mut data = ContentData::new(NativeNodeKind::MediaInline.into());
                    data.attrs_mut().insert_native(MediaSource::from(dest_uri));
                    data.attrs_mut().insert_native(MediaKind::Image);
                    if !title.is_empty() {
                        data.attrs_mut().insert_native(LinkTitle::from(title));
                    }
                    let new = current_parent.create_as_last_child(data);
                    open_elems.push(new);
                }
            },
            Event::End(tag) if is_sectioning_root_tag(&tag) => {
                // Close sectioning root and its children, if necessary.
                let closed = 'close_sectioning_root: loop {
                    let mut popped = None;
                    while let Some(popped_elem) = open_elems.pop() {
                        let kind = popped_elem.borrow_data().native_kind();
                        if kind == Some(NativeNodeKind::TransparentContainer) {
                            // The document root. Put it always bottom of the stack.
                            open_elems.push(popped_elem);
                            let popped = popped.expect("[consistency] some node must be closed");
                            break 'close_sectioning_root popped;
                        }
                        if kind.map_or(false, NativeNodeKind::is_sectioning_content_or_root) {
                            // Found a sectioning element.
                            popped = Some(popped_elem);
                            break;
                        }
                    }
                    assert!(
                        !open_elems.is_empty(),
                        "[consistency] `open_elems` should contain the body root"
                    );
                    if let Some((None, _closed)) = open_sections.pop() {
                        // A sectioning root was popped.
                        let popped = popped.expect("[consistency] some node must be closed");
                        break popped;
                    }
                };
                assert!(
                    !open_elems.is_empty(),
                    "[consistency] `open_elems` should contain the body root"
                );
                let kind = closed.borrow_data().native_kind();
                if kind == Some(NativeNodeKind::QuoteBlock) {
                    // If the node has single `Section` element, merge it to
                    // the quote block itself.
                    if closed.num_children() == 1 {
                        let child = closed
                            .first_child()
                            .expect("[validity] `closed` should have a child");
                        let kind = child.borrow_data().native_kind();
                        if kind == Some(NativeNodeKind::Section) {
                            child.replace_with_children();
                        }
                    }
                }
            }
            Event::End(Tag::TableHead) => {
                // Close `TableRow`.
                open_elems
                    .pop()
                    .expect("[consistency] `open_elems` should contain the body root");
                // Close `TableHeader`.
                open_elems
                    .pop()
                    .expect("[consistency] `open_elems` should contain the body root");
                assert!(
                    !open_elems.is_empty(),
                    "[consistency] `open_elems` should contain the body root"
                );
            }
            Event::End(Tag::Table(..)) => {
                // Close `TableBody` if necessary.
                if current_parent.borrow_data().native_kind() != Some(NativeNodeKind::Table) {
                    open_elems
                        .pop()
                        .expect("[consistency] `open_elems` should contain the body root");
                }
                // Close `Table`.
                open_elems
                    .pop()
                    .expect("[consistency] `open_elems` should contain the body root");
                assert!(
                    !open_elems.is_empty(),
                    "[consistency] `open_elems` should contain the body root"
                );
            }
            Event::End(_tag) => {
                open_elems
                    .pop()
                    .expect("[consistency] `open_elems` should contain the body root");
                assert!(
                    !open_elems.is_empty(),
                    "[consistency] `open_elems` should contain the body root"
                );
            }
            Event::Text(s) => {
                let mut done = false;
                if let Some(last_child) = current_parent.last_child() {
                    if last_child.borrow_data().is_text() {
                        // Write to the text node.
                        last_child.borrow_data_mut().append_text(&s);
                        done = true;
                    }
                }
                if !done {
                    // Create a new text node.
                    current_parent
                        .create_as_last_child(ContentData::new_text_from_pcmark_cowstr(s));
                }
            }
            Event::Code(text) => {
                let new = current_parent
                    .create_as_last_child(ContentData::new(NativeNodeKind::CodeInline.into()));
                new.create_as_last_child(ContentData::new_text_from_pcmark_cowstr(text));
            }
            Event::Html(text) => {
                let mut data = ContentData::new(NativeNodeKind::RawFragment.into());
                data.attrs_mut().insert_native(RawFragmentFormat::Html);
                let new = current_parent.create_as_last_child(data);
                new.create_as_last_child(ContentData::new_text_from_pcmark_cowstr(text));
            }
            Event::FootnoteReference(marker) => {
                let mut data = ContentData::new(NativeNodeKind::FootnoteReference.into());
                data.attrs_mut()
                    .insert_native(FootnoteId::from(marker.clone()));
                let new = current_parent.create_as_last_child(data);
                new.create_as_last_child(ContentData::new_text_from_pcmark_cowstr(marker));
            }
            Event::SoftBreak => {
                let mut done = false;
                if let Some(last_child) = current_parent.last_child() {
                    if last_child.borrow_data().is_text() {
                        // Write to the text node.
                        last_child.borrow_data_mut().append_text("\n");
                        done = true;
                    }
                }
                if !done {
                    // Create a new text node.
                    current_parent.create_as_last_child(ContentData::new_text_from_str("\n"));
                }
            }
            Event::HardBreak => {
                current_parent
                    .create_as_last_child(ContentData::new(NativeNodeKind::ForcedLineBreak.into()));
            }
            Event::Rule => {
                current_parent
                    .create_as_last_child(ContentData::new(NativeNodeKind::ThematicBreak.into()));
            }
            Event::TaskListMarker(_) => {
                // TODO: Add a new node kind.
                current_parent
                    .create_as_last_child(ContentData::new(NativeNodeKind::Unprocessable.into()));
            }
        }
    }
    body_root.tree()
}

/// Returns whether the node can be a (unwrappable) wrapper of inline nodes.
#[must_use]
fn may_wrap_inline(kind: NativeNodeKind) -> bool {
    matches!(
        kind,
        NativeNodeKind::Paragraph | NativeNodeKind::Section | NativeNodeKind::TransparentContainer
    )
}

/// Returns true if all of the children are inline element or text.
#[must_use]
fn all_children_inline(children: dendron::traverse::SiblingsTraverser<ContentData>) -> bool {
    for child in children {
        let data = child.borrow_data();
        let kind = match data.native_kind() {
            Some(v) => v,
            None => return false,
        };
        match kind.render_type() {
            RenderType::Inline => {}
            RenderType::Block => return false,
            RenderType::Contextual => {
                if !all_children_inline(child.children()) {
                    return false;
                }
            }
            RenderType::NotApplicable => return false,
        }
    }
    true
}

/// Reads a markdown document from the string as inline elements.
///
/// Returns `Ok(_)` if the content is readable as inline, `Err(_)` if not.
pub fn read_as_inline_content(s: &str, all_extensions: bool) -> Result<ContentTree, ContentTree> {
    let block_tree = read_as_block_content(s, all_extensions);
    let mut current = block_tree
        .root()
        .bundle_new_hierarchy_edit_grant()
        .expect("[consistency] new tree and referred only from here");
    loop {
        let mut data = current.borrow_data_mut();
        if data.is_text() {
            drop(data);
            // Text node can be considered inline.
            current.detach_subtree();
            break Ok(current.tree());
        }

        let native_kind = match data.native_kind() {
            Some(v) => v,
            None => break Err(block_tree),
        };
        let render_type = native_kind.render_type();
        if render_type == RenderType::Inline {
            current.detach_subtree();
            break Ok(current.tree());
        } else if render_type == RenderType::NotApplicable {
            break Err(block_tree);
        } else if may_wrap_inline(native_kind) {
            let num_children = current.num_children();
            if num_children == 0 {
                break Ok(ContentNode::new_tree(ContentData::new(
                    NativeNodeKind::TransparentContainer.into(),
                ))
                .tree());
            } else if num_children == 1 {
                let child = current
                    .first_child()
                    .expect("[consistency] `current` has a child");
                drop(data);
                current = child;
                continue;
            } else if all_children_inline(current.children()) {
                current.detach_subtree();
                *data = ContentData::new(NativeNodeKind::TransparentContainer.into());
                break Ok(current.tree());
            } else {
                break Err(block_tree);
            }
        } else {
            break Err(block_tree);
        }
    }
}

/// Returns `true` if the node with the given tag is a sectioning root.
#[inline]
#[must_use]
fn is_sectioning_root_tag(tag: &Tag<'_>) -> bool {
    matches!(tag, Tag::BlockQuote)
}
